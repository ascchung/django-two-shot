from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

# Register your models here.

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass

@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    pass
